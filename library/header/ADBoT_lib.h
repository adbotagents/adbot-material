/* ============================================================================

 UDP communication function

 Author: Gastone Pietro Rosati Papini

 ============================================================================ */

#ifndef __COMUNICATION_LIB_H
#define __COMUNICATION_LIB_H

#if defined(_WIN32)
#pragma comment (lib, "Ws2_32.lib")
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Winsock2.h>
#include <Ws2tcpip.h>

#elif defined(__MACH__) || defined(__linux__)
#include <arpa/inet.h>
#include <sys/socket.h>
#endif

#include "interfaces_data_structs.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef DLL_EXPORT
#define USE_DLL __declspec(dllexport)
#elif DLL_IMPORT
#define USE_DLL __declspec(dllimport)
#else
#define USE_DLL
#endif

// Comunication to simulator for agent init for matlab
USE_DLL	int comunication_init_num(unsigned int *num_ip = NULL, int server_port = 0);

// Comunication to simulator for agent init
// log_on == 0 log disabled
// log_on != 0 log enabled
USE_DLL int comunication_init(const char *server_ip = NULL, int server_port = 0);

// Send manoeuvre to simulator
USE_DLL int comunication_send(volatile uint32_t server_run, uint32_t message_id, output_data_str* manoeuvre_msg);

// Receive scenario from simulator
USE_DLL int comunication_receive(uint32_t *server_run, uint32_t *message_id, input_data_str* scenario_msg);

// Close comunication channel
USE_DLL int comunication_close();

// Client Get time
USE_DLL uint64_t client_get_time_ms();

#ifdef __cplusplus
}
#endif

#endif
