//
// Created by tonegas on 04/04/2019.
// This software is released under the AGPLv3 license. See LICENSE file for more information.
//

#include <iostream>
#include <chrono>
#include <string>

float calc_time_spent(std::chrono::system_clock::time_point start);

void printLine();

void printCodriver();

void printCodriverVersion(int codriverVersionMajor, int codriverVersionMinor, int codriverVersionInterfaces);

void printCenter(std::string str);

void printTableSection(std::string str);

void printTable(std::string str, int level);

template <class T>
void printTableAlign(std::string str, T value, int level){
    int pos = 70;
    std::string line = ">";
    for (int i = 0; i < 2; i++){
        line += " ";
    }
    if(level != 0){
        for (int i = 0; i < level*2; i++){
            line += " ";
        }
        line += "- ";
    }
    line += str;
    std::cout << line <<":" ;
    std::cout.width((pos-line.length()-1));
    std::cout << value;
    std::cout.width(81-pos);
    std::cout << "<\n";
}

template <class T>
void printTableAlignError(std::string str, T value, int level, std::string error="WRONG"){
    int pos = 70;
    std::string line = ">";
    for (int i = 0; i < 2; i++){
        line += " ";
    }
    if(level != 0){
        for (int i = 0; i < level*2; i++){
            line += " ";
        }
        line += "- ";
    }
    line += str;
    std::cout << line <<":" ;
    std::cout.width((pos-line.length()-1));
    std::cout << value;
    std::cout.width(81-pos);
    std::cout << error+"!<\n";
}

void printError(std::string str);

void printLog(int id, std::string message);

void printLogTitle(int id, std::string message);

template <class T>
void printLogVar(int id, std::string message, T var){
    static auto start = std::chrono::system_clock::now();
    printf("[%10f][%5d] ",calc_time_spent(start),id);
    std::cout << message << " = " << var << "\n";
}