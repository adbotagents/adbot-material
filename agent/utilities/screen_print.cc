//
// Created by tonegas on 04/04/2019.
// This software is released under the AGPLv3 license. See LICENSE file for more information.
//

#include <iostream>
#include <string>
#include "screen_print.h"
using namespace std;

void printLine(){
    cout<<"================================================================================\n";
}

void printCodriver(){
    cout<<
             "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n"
             "                      ______          __     _                   \n"
             "                     / ____/___  ____/ /____(_)   _____  _____   \n"
             "                    / /   / __ \\/ __  / ___/ / | / / _ \\/ ___/ \n"
             "                   / /___/ /_/ / /_/ / /  / /| |/ /  __/ /       \n"
             "                   \\____/\\____/\\__,_/_/  /_/ |___/\\___/_/    \n"
             "\n"
             ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n";
}

void printCenter(std::string str){
    cout.width((80-str.length())/2+str.length());
    cout << str << '\n';
}

void printCodriverVersion(int codriverVersionMajor, int codriverVersionMinor, int codriverVersionInterfaces){
    string str = "Co-driver version: ";
    str+=to_string(codriverVersionMajor) + ".";
    str+=to_string(codriverVersionMinor) + ".";
    str+=to_string(codriverVersionInterfaces);
    printLine();
    printCenter(str);
    printLine();
}

void printError(string str){
    cerr.fill('!');
    cerr.width((80-str.length())/2+str.length());
    cerr << str;
    cerr.fill('!');
    cerr.width((81-str.length())/2);
    cerr << '!' << '\n';
}

void printTableSection(string str){
    string line = ">";
    for (int i = 0; i < 2; i++){
        line += "-";
    }
    line += ' ' + str + ' ';
    cout << line ;
    cout.width((81-line.length()));
    cout.fill('-');
    cout << "<\n";
    cout.fill(' ');
}


void printTable(string str, int level){
    string line = ">";
    for (int i = 0; i < 2; i++){
        line += " ";
    }
    if(level != 0){
        for (int i = 0; i < level*2; i++){
            line += " ";
        }
        line += "- ";
    }
    line += str;
    cout << line;
    cout.width((81-line.length()));
    cout << "<\n";
}

float calc_time_spent(std::chrono::system_clock::time_point start){
    auto now = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = now - start;
    return (float)diff.count();
}

void printLog(int id, string message){
    static auto start = std::chrono::system_clock::now();
    cout << "[" ;
    cout.width(10);
    cout << calc_time_spent(start) << "]";
    cout.width(0);
    cout<<"[";
    cout.width(5);
    cout << id << "] ";
    cout.width(0);
    cout << message << "\n";
}

void printLogTitle(int id, string message){
    static auto start = std::chrono::system_clock::now();
    printf("[%10f][%5d] ",calc_time_spent(start),id);
    printf("------------------%*s%*s------------------\n",(int)(12+message.length()/2),message.c_str(),(int)(12-message.length()/2),"");
}


