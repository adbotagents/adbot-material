
/* ===============================================================================

  ADBoT agent example

  Authors: Gastone Pietro Rosati Papini and Riccardo Donà

 =============================================================================== */
#include "screen_print.h"
#include "ADBoT_lib.h"

#include "logvars.h"

#define DEFAULT_SERVER_IP  "127.0.0.1"
#define SERVER_PORT             30000  // Server port

// Handler for CTRL-C
#include <signal.h>
static uint32_t server_run = 1;
void intHandler(int signal) {
	server_run = 0;
}

int main(int argc, const char * argv[])
{
    // Log Manager
    LogManager log = LogManager();
    log.enable(true);

	// Messages variables
	scenario_msg_t  scenario_msg;
	manoeuvre_msg_t manoeuvre_msg;
	size_t scenario_msg_size  = sizeof(scenario_msg.data_buffer);
	size_t manoeuvre_msg_size = sizeof(manoeuvre_msg.data_buffer);
	uint32_t message_id = 0;

#ifndef _MSC_VER
	// More portable way of supporting signals on UNIX
	struct sigaction act;
	act.sa_handler = intHandler;
	sigaction(SIGINT, &act, NULL);
#else
	signal(SIGINT, intHandler);
#endif

	auto start = std::chrono::system_clock::now();
	int out = comunication_init(DEFAULT_SERVER_IP,SERVER_PORT);
	if(out == -2){
		printf("The system is already initialized.\n");
		exit(EXIT_FAILURE);
	}
	if(out == -1){
		printf("Open socket failed\n");
		exit(EXIT_FAILURE);
	}

	// Start server
    printLine();
    printTable("Waiting for scenario message...",0);
    printLine();
	while(server_run == 1) {

		// Clean the buffer
		memset(scenario_msg.data_buffer, '\0', scenario_msg_size);
		if (comunication_receive(&server_run, &message_id, &scenario_msg.data_struct) == 0) {

			printLogTitle(message_id, "received message");
			printLogVar(message_id, "ID", scenario_msg.data_struct.ID );
			printLogVar(message_id, "ALgtFild", scenario_msg.data_struct.ALgtFild);
			printLogVar(message_id, "CycleNumber", scenario_msg.data_struct.CycleNumber);
			printLogVar(message_id, "Current velocity [m/s]", scenario_msg.data_struct.VLgtFild);

			// Longitudinal control
			manoeuvre_msg.data_struct.RequestedAcc = 0.5*(scenario_msg.data_struct.RequestedCruisingSpeed-scenario_msg.data_struct.VLgtFild);

			// Log test
			log.log_var("data","time",scenario_msg.data_struct.ECUupTime);
			log.log_var("data","vlgt",scenario_msg.data_struct.VLgtFild);
			log.write_line("data");

			// Check values
			manoeuvre_msg.data_struct.ID = scenario_msg.data_struct.ID;
			manoeuvre_msg.data_struct.CycleNumber = scenario_msg.data_struct.CycleNumber;
			manoeuvre_msg.data_struct.ECUupTime = calc_time_spent(start);
			manoeuvre_msg.data_struct.Status = scenario_msg.data_struct.Status;
			manoeuvre_msg.data_struct.Version = 0; // 0 lateral control activated
			manoeuvre_msg.data_struct.RequestedSteerWhlAg = 0.;

			if (comunication_send(server_run, message_id, &manoeuvre_msg.data_struct) == -1) {
				perror("error send_message()");
				exit(EXIT_FAILURE);
			}else{
                printLogTitle(message_id, "sent message");
			}
		}
	}

	printLine();
	printTable("Stopping server...",0);
	if(comunication_close() == -1) {
		exit(EXIT_FAILURE);
	}
	printTable("Done",0);
	printLine();
	return 0;
}
